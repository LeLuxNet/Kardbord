package postnord

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"lelux.net/kardbord"
)

func TestTracking(t *testing.T) {
	ss := PostNord{}

	s, err := ss.TrackSingle("UI307522835SE")
	assert.Nil(t, err)
	assert.NotNil(t, s)

	es := s.Events
	assert.Equal(t, 4, len(es))

	e := es[0]
	assert.Equal(t, kardbord.StatusProcessing, e.Status)
	assert.Equal(t, "We have received a notification from your shipper that they are preparing an item for you. The tracking information will be updated when the parcel is handed over to PostNord.", e.Message)
	assert.Equal(t, time.Date(2021, 8, 30, 10, 26, 0, 0, time.UTC), e.Time)
	assert.Equal(t, "PostNord", e.Location)

	e = es[1]
	assert.Equal(t, kardbord.StatusTransit, e.Status)
	assert.Equal(t, "The shipment item is under transportation.", e.Message)
	assert.Equal(t, time.Date(2021, 8, 30, 17, 22, 0, 0, time.UTC), e.Time)
	assert.Equal(t, "Helsingborg 25464, Sweden", e.Location)

	e = es[3]
	assert.Equal(t, kardbord.StatusDelivered, e.Status)
	assert.Equal(t, "The shipment item has been delivered.", e.Message)
	assert.Equal(t, time.Date(2021, 9, 1, 11, 53, 21, 0, time.UTC), e.Time)
	assert.Equal(t, "Aalborg 9000, Denmark", e.Location)

	assert.Equal(t, 0.26, s.Length)
	assert.Equal(t, 0.16, s.Width)
	assert.Equal(t, 0.07, s.Height)

	assert.Equal(t, 0.3, s.Weight)
}

func TestTrackingMissing(t *testing.T) {
	ss := PostNord{}

	s, err := ss.TrackSingle("x")
	assert.Nil(t, err)
	assert.Nil(t, s)
}
