package laposte

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strings"
	"sync"
	"time"

	"lelux.net/kardbord"
)

var _ kardbord.SingleTrackable = (*LaPoste)(nil)

type LaPoste struct {
	Key string

	key     string
	keyLock sync.Mutex
}

func (ss *LaPoste) getKey() (string, error) {
	if ss.Key != "" {
		return ss.Key, nil
	}

	ss.keyLock.Lock()
	defer ss.keyLock.Unlock()

	if ss.key != "" {
		return ss.key, nil
	}

	res, err := http.Get("https://www.laposte.fr/outils/track-a-parcel")
	if err != nil {
		return "", err
	}
	err = res.Body.Close()
	if err != nil {
		return "", err
	}

	for _, c := range res.Cookies() {
		if c.Name == "access_token" {
			rawBody, err := base64.StdEncoding.DecodeString(strings.Split(c.Value, ".")[1])
			if err != nil {
				return "", err
			}

			body := &accessToken{}
			err = json.Unmarshal(rawBody, body)
			if err != nil {
				return "", err
			}

			ss.key = body.Okapi.AppKey
			return ss.key, nil
		}
	}

	return "", http.ErrNoCookie
}

type accessToken struct {
	Okapi struct {
		AppKey string
	}
}

func (ss *LaPoste) TrackSingle(id string) (*kardbord.Shipment, error) {
	key, err := ss.getKey()
	if err != nil {
		return nil, err
	}

	lang := kardbord.Lang
	if lang == "en_US" {
		lang = "en_GB"
	}

	url := fmt.Sprintf("https://api.laposte.fr/suivi/v2/idships/%s?lang=%s", url.PathEscape(id), url.QueryEscape(lang))
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return nil, err
	}

	req.Header.Set("Accept", "application/json")
	req.Header.Set("X-Okapi-Key", key)

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	if res.StatusCode == http.StatusNotFound ||
		res.StatusCode == http.StatusBadRequest {
		return nil, nil
	}

	b, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	j := &response{}
	err = json.Unmarshal(b, j)
	if err != nil {
		return nil, err
	}
	jes := j.Shipment.Event

	e := make([]kardbord.Event, len(jes))
	for i, je := range jes {
		var status kardbord.Status
		switch je.Code[:2] {
		case "DR": // Delaration received
			status = kardbord.StatusProcessing
		case
			"PC",
			"ET":
			status = kardbord.StatusTransit
		case "EP":
		case "DO": // Customs
			if je.Code[2] == '3' { // Retained at customs
				status = kardbord.StatusFailure
			} else {
				status = kardbord.StatusTransit
			}
		case "PB": // Problem
			if je.Code[2] == '2' { // Problem resolved
				status = kardbord.StatusTransit
			} else {
				status = kardbord.StatusFailure
			}
		case
			"ND", // Not distributable
			"MD": // Missed delivery
			status = kardbord.StatusFailure
		case "AG": // Waiting for collection at counter
			status = kardbord.StatusTransit
		case "RE": // Returned
			status = kardbord.StatusReturned
		case "DI": // Distributed
			if je.Code[2] == '2' { // Distributed to sender after return
				status = kardbord.StatusReturned
			} else {
				status = kardbord.StatusDelivered
			}
		}

		t, err := time.Parse(time.RFC3339, je.Date)
		if err != nil {
			return nil, err
		}

		e[len(e)-1-i] = kardbord.Event{
			Status:  status,
			Message: je.Label,
			Time:    t,
		}
	}

	return &kardbord.Shipment{
		Events: e,
	}, nil
}

type response struct {
	Shipment struct {
		Event []struct {
			Label string
			Date  string
			Code  string
		}
	}
}
