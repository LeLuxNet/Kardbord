package australiapost

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"lelux.net/kardbord"
)

func TestTracking(t *testing.T) {
	ss := AustraliaPost{}

	s, err := ss.TrackMultiple("LH188178595AU", "xx")
	assert.Nil(t, err)
	assert.Equal(t, 2, len(s))

	assert.Equal(t, 14, len(s[0].Events))

	e := s[0].Events[0]
	assert.Equal(t, kardbord.StatusTransit, e.Status)
	assert.Equal(t, "Picked up from Sender", e.Message)
	assert.Equal(t, time.Date(2021, 9, 9, 4, 29, 41, 0, time.UTC), e.Time.UTC())
	assert.Equal(t, "EASTWOOD SA", e.Location)

	e = s[0].Events[4]
	assert.Equal(t, kardbord.StatusTransit, e.Status)
	assert.Equal(t, "Processed by air carrier", e.Message)
	assert.Equal(t, time.Date(2021, 9, 13, 22, 14, 0, 0, time.UTC), e.Time.UTC())
	assert.Equal(t, "", e.Location)

	assert.Nil(t, s[1])
}
