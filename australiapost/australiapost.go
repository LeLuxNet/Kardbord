package australiapost

import (
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"net/url"
	"strings"
	"time"

	"lelux.net/kardbord"
)

// https://auspost.com.au/mypost/track/set-assets-dir.js
// https://auspost.com.au/mypost/track/assets-[...]/mypost-track-config.js
// envApiKeys.shipmentsGatewayAPI
var apiKey = "d11f9456-11c3-456d-9f6d-f7449cb9af8e"

// If this token gets invalid:
// This token is available as a cookie in the tracking response when ErrCaptcha is thrown
var dataDome = "JMGJh9s4.cqfDARjZKsju0zq66Mt7IdLlGjy5V8ZVLGch86Py.DOBYfOejzncLPlr7Up2T7biBfD9daVqv-X850A5ywj6AfHXRwL3PcMIt"

var (
	ErrCaptcha = errors.New("a captcha was requested by Australia Post. This does not occur under normal circumstances and is probably caused by an API change")
)

var _ kardbord.MultipleTrackable = (*AustraliaPost)(nil)

type AustraliaPost struct{}

func (ss *AustraliaPost) TrackMultiple(ids ...string) ([]*kardbord.Shipment, error) {
	url := "https://digitalapi.auspost.com.au/shipmentsgatewayapi/watchlist/shipments?trackingIds=" + url.QueryEscape(strings.Join(ids, ","))
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return nil, err
	}

	req.Header.Set("api-key", apiKey)
	req.AddCookie(&http.Cookie{
		Name:  "datadome",
		Value: dataDome,
	})

	req.Header.Set("Accept", "application/json, text/plain, */*")
	req.Header.Set("Accept-Language", "en_US")
	req.Header.Set("Referer", "https://auspost.com.au")
	req.Header.Set("User-Agent", kardbord.BrowserUserAgent)

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	b, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	if res.StatusCode != http.StatusOK {
		j := &responseCaptcha{}
		err = json.Unmarshal(b, &j)
		if err != nil {
			return nil, http.ErrAbortHandler
		} else {
			return nil, ErrCaptcha
		}
	}

	j := []response{}
	err = json.Unmarshal(b, &j)
	if err != nil {
		return nil, err
	}

	s := make([]*kardbord.Shipment, len(ids))
	for i, js := range j {
		if js.Shipment == nil {
			continue
		}

		jes := js.Shipment.Articles[0].Details[0].Events

		e := make([]kardbord.Event, len(jes))
		for i, je := range jes {
			var status kardbord.Status
			switch je.EventCode[:strings.IndexByte(je.EventCode, '-')] {
			case
				"AFC",
				"INT":
				status = kardbord.StatusTransit
			}

			e[len(jes)-i-1] = kardbord.Event{
				Status:   status,
				Message:  je.Description,
				Time:     time.Unix(0, je.DateTime*1e6),
				Location: je.Location,
			}
		}

		s[i] = &kardbord.Shipment{
			Events: e,
		}
	}

	return s, nil
}

type response struct {
	Shipment *struct {
		Articles []struct {
			Details []struct {
				Events []struct {
					DateTime    int64
					Description string
					Location    string
					EventCode   string
				}
			}
		}
	}
}

type responseCaptcha struct {
	URL string
}
