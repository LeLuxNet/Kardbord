package hermes

import (
	"encoding/json"
	"io"
	"net/http"
	"net/url"
	"strings"
	"time"

	"lelux.net/kardbord"
)

var _ kardbord.SingleTrackable = (*Hermes)(nil)

type Hermes struct{}

func (ss *Hermes) TrackSingle(id string) (*kardbord.Shipment, error) {
	res, err := http.Get("https://www.myhermes.de/services/tracking/shipments?search=" + url.QueryEscape(id))
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	if res.StatusCode == http.StatusUnprocessableEntity {
		return nil, nil
	}

	b, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	j := []response{}
	err = json.Unmarshal(b, &j)
	if err != nil {
		return nil, err
	}

	js := j[0]
	jes := js.StatusHistory

	e := make([]kardbord.Event, len(jes))
	for i, je := range jes {
		var status kardbord.Status
		if i == 0 {
			switch j[0].StatusImageID {
			case 0:
				status = kardbord.StatusProcessing
			case 1, 2, 3, 4:
				status = kardbord.StatusTransit
			case 5:
				status = kardbord.StatusDelivered
			}
		}

		t, err := time.Parse("02.01.2006 15:04", je.DateTime)
		if err != nil {
			return nil, err
		}

		e[len(jes)-i-1] = kardbord.Event{
			Status:  status,
			Message: strings.TrimSpace(je.Description),
			Time:    t,
		}
	}

	return &kardbord.Shipment{
		Events: e,
	}, nil
}

type response struct {
	StatusImageID int
	StatusHistory []struct {
		Description string
		DateTime    string
	}
	ExpectedDate string
}
