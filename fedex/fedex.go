package fedex

import (
	"encoding/json"
	"io"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"lelux.net/kardbord"
)

var _ kardbord.MultipleTrackable = (*FedEx)(nil)

type FedEx struct{}

func (ss *FedEx) TrackMultiple(ids ...string) ([]*kardbord.Shipment, error) {
	reqDataList := make([]trackingRequest, len(ids))
	for i, id := range ids {
		reqDataList[i] = trackingRequest{
			Info: trackingRequestInfo{
				Number: id,
			}}
	}

	reqData, err := json.Marshal(&request{
		Request: requestRequest{
			TrackingList: reqDataList,
		}})
	if err != nil {
		return nil, err
	}

	reqB := url.Values{}
	reqB.Set("action", "trackpackages")
	reqB.Set("data", string(reqData))
	reqB.Set("format", "json")
	reqB.Set("locale", kardbord.Lang)
	reqB.Set("version", "1")

	res, err := http.Post("https://www.fedex.com/trackingCal/track", "application/x-www-form-urlencoded", strings.NewReader(reqB.Encode()))
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	b, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	j := &response{}
	err = json.Unmarshal(b, j)
	if err != nil {
		return nil, err
	}

	s := make([]*kardbord.Shipment, len(ids))
	for i, js := range j.Response.PackageList {
		if !js.IsSuccessful {
			continue
		}

		jes := js.Events

		e := make([]kardbord.Event, len(jes))
		for i, je := range jes {
			var status kardbord.Status
			switch je.StatusCode {
			case
				"OC": //
				status = kardbord.StatusProcessing
			case
				"AF", // At facility
				"AR", // Arrived
				"CC", //
				"DP", // Departed
				"IT", // In transit
				"PU", // Picked up
				"OD", // On delivery
				"DY": // Delay
				status = kardbord.StatusTransit
			case
				"DL": // Delivered
				status = kardbord.StatusDelivered
			}

			t, err := time.Parse("2006-01-0215:04:05-07:00", je.Date+je.Time+je.GmtOffset)
			if err != nil {
				return nil, err
			}

			e[len(jes)-i-1] = kardbord.Event{
				Status:   status,
				Message:  je.Status,
				Time:     t,
				Location: je.ScanLocation,
			}
		}

		var length, width, height float64
		dimParts := strings.Split(js.Dimensions[:strings.IndexByte(js.Dimensions, ' ')], "x")
		if len(dimParts) == 3 {
			length, _ = strconv.ParseFloat(dimParts[0], 64)
			width, _ = strconv.ParseFloat(dimParts[1], 64)
			height, _ = strconv.ParseFloat(dimParts[2], 64)
		}

		weight, _ := strconv.ParseFloat(js.Weight, 64)

		s[i] = &kardbord.Shipment{
			Events: e,

			Length: length / 100,
			Width:  width / 100,
			Height: height / 100,

			Weight: weight,
		}
	}

	return s, nil
}

type request struct {
	Request requestRequest `json:"TrackPackagesRequest"`
}

type requestRequest struct {
	TrackingList []trackingRequest `json:"trackingInfoList"`
}

type trackingRequest struct {
	Info trackingRequestInfo `json:"trackNumberInfo"`
}

type trackingRequestInfo struct {
	Number string `json:"trackingNumber"`
}

type response struct {
	Response struct {
		PackageList []struct {
			Weight     string `json:"pkgKgsWgt"`
			Dimensions string `json:"pkgDimCm"`
			Events     []struct {
				Date         string
				Time         string
				GmtOffset    string
				Status       string
				StatusCode   string `json:"statusCD"`
				ScanLocation string
				ScanDetails  string
			} `json:"scanEventList"`
			IsSuccessful bool
		}
	} `json:"TrackPackagesResponse"`
}
