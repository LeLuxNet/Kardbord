package fedex

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"lelux.net/kardbord"
)

func TestTracking(t *testing.T) {
	ss := FedEx{}

	s, err := ss.TrackMultiple("282772025784", "x")
	assert.Nil(t, err)
	assert.Equal(t, 2, len(s))

	assert.Equal(t, 26, len(s[0].Events))

	e := s[0].Events[0]
	assert.Equal(t, kardbord.StatusProcessing, e.Status)
	assert.Equal(t, "Shipment information sent to FedEx", e.Message)
	assert.Equal(t, time.Date(2021, 8, 20, 5, 29, 50, 0, time.UTC), e.Time.UTC())
	assert.Equal(t, "", e.Location)

	e = s[0].Events[8]
	assert.Equal(t, kardbord.StatusTransit, e.Status)
	assert.Equal(t, "In transit", e.Message)
	assert.Equal(t, time.Date(2021, 8, 22, 20, 58, 0, 0, time.UTC), e.Time.UTC())
	assert.Equal(t, "GUANGZHOU CN", e.Location)

	assert.Equal(t, 0.508, s[0].Length)
	assert.Equal(t, 0.2286, s[0].Width)
	assert.Equal(t, 0.1524, s[0].Height)

	assert.Equal(t, 2.86, s[0].Weight)

	assert.Nil(t, s[1])
}
