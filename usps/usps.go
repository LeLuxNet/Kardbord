package usps

import (
	"encoding/xml"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strings"
	"time"

	"lelux.net/kardbord"
)

var (
	ErrNoUserID = errors.New("not authenticated using a user ID. Register on https://registration.shippingapis.com")
)

var _ kardbord.MultipleTrackable = (*USPS)(nil)

type USPS struct {
	UserID string
}

func (ss *USPS) TrackMultiple(ids ...string) ([]*kardbord.Shipment, error) {
	if ss.UserID == "" {
		return nil, ErrNoUserID
	}

	reqDataList := make([]trackingRequest, len(ids))
	for i, id := range ids {
		reqDataList[i] = trackingRequest{
			ID: id,
		}
	}

	reqB, err := xml.Marshal(&request{
		UserID:   ss.UserID,
		Revision: 1,
		ClientIP: "127.0.0.1",
		SourceID: "-",
		TrackID:  reqDataList,
	})
	if err != nil {
		return nil, err
	}

	res, err := http.Get("https://secure.shippingapis.com/ShippingAPI.dll?API=TrackV2&XML=" + url.QueryEscape(string(reqB)))
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	b, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	j := &response{}
	err = xml.Unmarshal(b, j)
	if err != nil {
		return nil, err
	}

	s := make([]*kardbord.Shipment, len(ids))
	for i, js := range j.Info {
		if js.Error != nil {
			if js.Error.Number == -2147219302 {
				continue
			} else {
				return nil, fmt.Errorf("%s (%d)", js.Error.Description, js.Error.Number)
			}
		}

		jes := js.Detail

		e := make([]kardbord.Event, len(*jes))
		for i, je := range *jes {

			// https://about.usps.com/publications/pub97/pub97_appi.htm
			var status kardbord.Status
			switch je.Code {
			case
				"MA": // Manifest Acknowledgment
				status = kardbord.StatusProcessing
			case
				"TM", // Truck manifest

				"A1", // Arrived at facility
				"U1", // Arrived at regional facility

				"OA", // Accepted at origin facility

				"L1", // Departed at facility
				"T1", // Departed at regional facility

				"08", // Missent to incorrect facility
				"10": // Automated scan in a processing facility
				status = kardbord.StatusTransit
			case
				"01", // Delivered
				"07": // Arrival at final postal unit
				status = kardbord.StatusDelivered
			case
				"09": // Returned
				status = kardbord.StatusReturned
			}

			var t time.Time
			if je.Time == "" {
				t, err = time.Parse("January 2, 2006", je.Date)
			} else {
				t, err = time.Parse("January 2, 20063:04 pm", je.Date+je.Time)
			}
			if err != nil {
				return nil, err
			}

			var location strings.Builder
			location.WriteString(je.City)
			if je.State != "" || je.ZIPCode != "" {
				location.WriteString(", ")

				if je.State != "" {
					location.WriteString(je.State)
					location.WriteString(" ")
				}
				if je.ZIPCode != "" {
					location.WriteString(je.ZIPCode)
				}
			}
			if je.Country != "" {
				location.WriteString(", ")
				location.WriteString(je.Country)
			}

			e[len(*jes)-i-1] = kardbord.Event{
				Status:   status,
				Message:  je.Status,
				Location: location.String(),
				Time:     t,
			}
		}

		s[i] = &kardbord.Shipment{
			Events: e,
		}
	}

	return s, nil
}

type request struct {
	UserID   string `xml:"USERID,attr"`
	Revision int
	ClientIP string `xml:"ClientIp"`
	SourceID string `xml:"SourceId"`
	TrackID  []trackingRequest
	XMLName  struct{} `xml:"TrackFieldRequest"`
}

type trackingRequest struct {
	ID string `xml:",attr"`
}

type response struct {
	Info []struct {
		// ID     string `xml:"ID,attr"`
		Detail *[]struct {
			Time     string `xml:"EventTime"`
			Date     string `xml:"EventDate"`
			Status   string `xml:"Event"`
			City     string `xml:"EventCity"`
			State    string `xml:"EventState"`
			ZIPCode  string `xml:"EventZIPCode"`
			Country  string `xml:"EventCountry"`
			FirmName string
			Name     string
			Code     string `xml:"EventCode"`
		} `xml:"TrackDetail"`
		Error *struct {
			Number      int32
			Description string
		}
	} `xml:"TrackInfo"`
	XMLName struct{} `xml:"TrackResponse"`
}
