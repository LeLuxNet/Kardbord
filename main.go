package kardbord

import (
	"time"
)

var Lang = "en_US"

// Firefox 91.0
var BrowserUserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:91.0) Gecko/20100101 Firefox/91.0"

type Shipment struct {
	Events []Event `json:"events"`

	Length float64 `json:"length,omitempty"` // m
	Width  float64 `json:"width,omitempty"`  // m
	Height float64 `json:"height,omitempty"` // m

	Weight float64 `json:"weight,omitempty"` // kg
}

type Event struct {
	Status   Status    `json:"status"`
	Message  string    `json:"message"`
	Time     time.Time `json:"time"`
	Location string    `json:"location,omitempty"`
}

type SingleTrackable interface {
	TrackSingle(id string) (*Shipment, error)
}

type MultipleTrackable interface {
	TrackMultiple(ids ...string) ([]*Shipment, error)
}
