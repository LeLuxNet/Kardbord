package oesterreichischepost

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"
	"time"

	"lelux.net/kardbord"
)

var gqlQuery = `query ($id: String) {
    einzelsendung(sendungsnummer: $id) {
		sendungsnummer
		branchkey
		estimatedDelivery {
			startDate
			endDate
			startTime
			endTime
		}
		dimensions {
			height
			width
			length
		}
		status
		weight
		sendungsEvents {
			timestamp
			status
			reasontypecode
			text
			textEn
			eventpostalcode
			eventcountry
		}
		customsInformation {
			customsDocumentAvailable
			userDocumentNeeded
		}
	}
}`

var _ kardbord.SingleTrackable = (*OesterreichischePost)(nil)

type OesterreichischePost struct {
	Key string
}

func (ss *OesterreichischePost) TrackSingle(id string) (*kardbord.Shipment, error) {
	reqB, err := json.Marshal(&gqlRequest{
		Query: gqlQuery,
		Variables: map[string]interface{}{
			"id": id,
		},
	})
	if err != nil {
		return nil, err
	}

	res, err := http.Post("https://api.post.at/sendungen/sv/graphqlPublic", "application/json", bytes.NewReader(reqB))
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	if res.StatusCode == http.StatusUnprocessableEntity {
		return nil, nil
	}

	b, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	j := &response{}
	err = json.Unmarshal(b, j)
	if err != nil {
		return nil, err
	}
	js := j.Data.Shipment
	jes := js.Events

	e := make([]kardbord.Event, len(jes))
	for i, je := range jes {
		var status kardbord.Status
		switch je.Status {
		case "AN",
			"BEI",
			"EXP",
			"IMP",
			"BAU",
			"AZT":
			status = kardbord.StatusTransit
		case "IZ":
			status = kardbord.StatusDelivered
		}

		var msg string
		if strings.HasPrefix(kardbord.Lang, "de_") {
			msg = je.Text
		} else {
			msg = je.TextEn
		}

		t, err := time.Parse(time.RFC3339, je.Timestamp)
		if err != nil {
			return nil, err
		}

		e[i] = kardbord.Event{
			Status:   status,
			Message:  msg,
			Time:     t,
			Location: fmt.Sprintf("%s, %s", je.PostalCode, je.Country),
		}
	}

	return &kardbord.Shipment{
		Events: e,

		Length: float64(js.Dimensions.Length) / 100,
		Width:  float64(js.Dimensions.Width) / 100,
		Height: float64(js.Dimensions.Height) / 100,

		Weight: js.Weight,
	}, nil
}

type gqlRequest struct {
	Query     string                 `json:"query"`
	Variables map[string]interface{} `json:"variables"`
}

type response struct {
	Data struct {
		Shipment struct {
			Dimensions struct {
				Height int
				Width  int
				Length int
			}
			Weight float64
			Events []struct {
				Timestamp  string
				Status     string
				Text       string
				TextEn     string
				PostalCode string `json:"eventpostalcode"`
				Country    string `json:"eventcountry"`
			} `json:"sendungsEvents"`
		} `json:"einzelsendung"`
	}
}
