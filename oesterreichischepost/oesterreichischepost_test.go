package oesterreichischepost

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"lelux.net/kardbord"
)

func TestTracking(t *testing.T) {
	ss := OesterreichischePost{}

	s, err := ss.TrackSingle("CA338016374AT")
	assert.Nil(t, err)
	assert.NotNil(t, s)

	es := s.Events
	assert.Equal(t, 7, len(es))

	e := es[0]
	assert.Equal(t, kardbord.StatusTransit, e.Status)
	assert.Equal(t, "Item accepted", e.Message)
	assert.Equal(t, time.Date(2021, 9, 2, 7, 8, 11, 0, time.UTC), e.Time.UTC())
	assert.Equal(t, "3380, AT", e.Location)

	e = es[4]
	assert.Equal(t, kardbord.StatusTransit, e.Status)
	assert.Equal(t, "Item distributed", e.Message)
	assert.Equal(t, time.Date(2021, 9, 9, 15, 2, 1, 0, time.UTC), e.Time.UTC())
	assert.Equal(t, "GBCVTB, GB", e.Location)

	assert.Equal(t, 0.38, s.Length)
	assert.Equal(t, 0.17, s.Width)
	assert.Equal(t, 0.15, s.Height)

	assert.Equal(t, 3.235, s.Weight)
}

func TestTrackingMissing(t *testing.T) {
	ss := OesterreichischePost{}

	s, err := ss.TrackSingle("xxxxx")
	assert.Nil(t, err)
	assert.Nil(t, s)
}
