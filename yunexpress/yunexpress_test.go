package yunexpress

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"lelux.net/kardbord"
)

func TestTracking(t *testing.T) {
	ss := YunExpress{}

	s, err := ss.TrackMultiple("YT2120312102001004", "x")
	assert.Nil(t, err)
	assert.Equal(t, 2, len(s))

	assert.Equal(t, 18, len(s[0].Events))

	e := s[0].Events[0]
	assert.Equal(t, kardbord.StatusUnknown, e.Status)
	assert.Equal(t, "shipment information received", e.Message)
	assert.Equal(t, time.Date(2021, 7, 22, 17, 12, 2, 0, time.UTC), e.Time)
	assert.Equal(t, "", e.Location)

	e = s[0].Events[3]
	assert.Equal(t, kardbord.StatusUnknown, e.Status)
	assert.Equal(t, "departed facility in processing center", e.Message)
	assert.Equal(t, time.Date(2021, 7, 25, 11, 45, 8, 0, time.UTC), e.Time)
	assert.Equal(t, "shenzhen", e.Location)

	assert.Equal(t, 0.15, s[0].Weight)

	assert.Nil(t, s[1])
}
