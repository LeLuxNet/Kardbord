package yunexpress

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"strings"
	"time"

	"lelux.net/kardbord"
)

var _ kardbord.MultipleTrackable = YunExpress{}

type YunExpress struct{}

func (ss YunExpress) TrackMultiple(ids ...string) ([]*kardbord.Shipment, error) {
	reqB, err := json.Marshal(&request{NumberList: ids})
	if err != nil {
		return nil, err
	}

	res, err := http.Post("https://new.yuntrack.com/YunTrackAPI/Track/Query", "application/json", bytes.NewReader(reqB))
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	b, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	j := &response{}
	err = json.Unmarshal(b, j)
	if err != nil {
		return nil, err
	}

	s := make([]*kardbord.Shipment, len(ids))
	for i, js := range j.ResultList {
		if js.TrackData.TrackStatus == "" {
			continue
		}

		jes := js.TrackInfo.TrackEventDetails

		e := make([]kardbord.Event, len(jes))
		for i, je := range jes {
			var status kardbord.Status
			if i == len(jes)-1 {
				switch js.TrackData.TrackStatus {
				case "Processing":
					status = kardbord.StatusProcessing
				case "Transit":
					status = kardbord.StatusTransit
				case "Delivered":
					status = kardbord.StatusDelivered
				case "Returned":
					status = kardbord.StatusReturned
				}
			}

			var t time.Time
			t, err = time.Parse("2006-01-02T15:04:05", je.CreatedOn)
			if err != nil {
				return nil, err
			}

			e[i] = kardbord.Event{
				Status:   status,
				Message:  strings.TrimSpace(je.ProcessContent),
				Time:     t,
				Location: je.ProcessLocation,
			}
		}

		s[i] = &kardbord.Shipment{
			Events: e,

			Weight: js.TrackInfo.Weight,
		}
	}

	return s, nil
}

type request struct {
	NumberList []string
}

type response struct {
	ResultList []struct {
		TrackInfo struct {
			Weight            float64
			TrackEventDetails []struct {
				ProcessContent  string
				CreatedOn       string
				ProcessLocation string
			}
		}
		TrackData struct {
			TrackStatus string
		}
	}
}
