module lelux.net/kardbord/server

go 1.17

require (
	github.com/gorilla/mux v1.8.0
	lelux.net/kardbord v0.1.2
)
