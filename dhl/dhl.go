package dhl

import (
	"encoding/json"
	"io"
	"net/http"
	"net/url"
	"time"

	"lelux.net/kardbord"
)

const DefaultKey = "demo-key"
const KeyHeader = "DHL-API-Key"

var _ kardbord.SingleTrackable = (*DHL)(nil)

type DHL struct {
	Key string
}

func (ss *DHL) TrackSingle(id string) (*kardbord.Shipment, error) {
	req, err := http.NewRequest(http.MethodGet, "https://api-eu.dhl.com/track/shipments?trackingNumber="+url.QueryEscape(id), nil)
	if err != nil {
		return nil, err
	}

	if ss.Key == "" {
		req.Header.Add(KeyHeader, DefaultKey)
	} else {
		req.Header.Add(KeyHeader, ss.Key)
	}

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	if res.StatusCode == http.StatusBadRequest {
		return nil, nil
	}

	b, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	j := &response{}
	err = json.Unmarshal(b, j)
	if err != nil {
		return nil, err
	}
	js := j.Shipments[0]
	jes := js.Events

	e := make([]kardbord.Event, len(jes))
	for i, je := range jes {
		var status kardbord.Status
		switch je.StatusCode {
		case "pre-transit":
			status = kardbord.StatusProcessing
		case "transit":
			status = kardbord.StatusTransit
		case "delivered":
			status = kardbord.StatusDelivered
		case "failure":
			status = kardbord.StatusFailure
		}

		t, err := time.Parse("2006-01-02T15:04:05", je.Timestamp)
		if err != nil {
			return nil, err
		}

		e[len(e)-1-i] = kardbord.Event{
			Status:  status,
			Message: je.Status,
			Time:    t,
		}
	}

	s := &kardbord.Shipment{
		Events: e,
	}

	if js.Details.Dimensions.Length.Unit == "m" {
		s.Length = js.Details.Dimensions.Length.Value
	}
	if js.Details.Dimensions.Width.Unit == "m" {
		s.Width = js.Details.Dimensions.Width.Value
	}
	if js.Details.Dimensions.Height.Unit == "m" {
		s.Height = js.Details.Dimensions.Height.Value
	}

	if js.Details.Weight.Unit == "kg" {
		s.Weight = js.Details.Weight.Value
	}

	return s, nil
}

type response struct {
	Shipments []struct {
		Details struct {
			Weight     measurement
			Dimensions struct {
				Width  measurement
				Height measurement
				Length measurement
			}
		}
		Events []struct {
			Timestamp  string
			StatusCode string
			Status     string
		}
	}
}

type measurement struct {
	Value float64
	Unit  string `json:"unitText"`
}
