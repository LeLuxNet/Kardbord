package dhl

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"lelux.net/kardbord"
)

func TestTracking(t *testing.T) {
	ss := DHL{}

	s, err := ss.TrackSingle("00340434292135100148")
	assert.Nil(t, err)
	assert.NotNil(t, s)

	es := s.Events
	assert.Equal(t, 1, len(es))

	e := es[0]
	assert.Equal(t, kardbord.StatusProcessing, e.Status)
	assert.Equal(t, "The instruction data for this shipment have been provided by the sender to DHL electronically", e.Message)
	assert.Equal(t, time.Date(2021, 7, 6, 19, 35, 0, 0, time.UTC), e.Time)
	assert.Equal(t, "", e.Location)

	assert.Equal(t, 0.38, s.Length)
	assert.Equal(t, 0.3, s.Width)
	assert.Equal(t, 0.14, s.Height)

	assert.Equal(t, 2.0, s.Weight)
}

func TestTrackingMissing(t *testing.T) {
	ss := DHL{}

	s, err := ss.TrackSingle("x")
	assert.Nil(t, err)
	assert.Nil(t, s)
}
