package dpd

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"lelux.net/kardbord"
)

func TestTracking(t *testing.T) {
	ss := DPD{}

	s, err := ss.TrackSingle("01287055774315")
	assert.Nil(t, err)
	assert.NotNil(t, s)

	es := s.Events
	assert.Equal(t, 6, len(es))

	e := es[0]
	assert.Equal(t, kardbord.StatusProcessing, e.Status)
	assert.Equal(t, "Order information has been transmitted to DPD.", e.Message)
	assert.Equal(t, time.Date(2021, 7, 12, 16, 53, 7, 0, time.UTC), e.Time)
	assert.Equal(t, "DPD data centre", e.Location)

	e = es[3]
	assert.Equal(t, kardbord.StatusTransit, e.Status)
	assert.Equal(t, "At parcel delivery centre.", e.Message)
	assert.Equal(t, time.Date(2021, 7, 14, 5, 40, 44, 0, time.UTC), e.Time)
	assert.Equal(t, "Obing (DE)", e.Location)
}

func TestTrackingMissing(t *testing.T) {
	ss := DPD{}

	s, err := ss.TrackSingle("x")
	assert.Nil(t, err)
	assert.Nil(t, s)
}
