package gls

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"lelux.net/kardbord"
)

var _ kardbord.SingleTrackable = (*GLS)(nil)

type GLS struct{}

func (ss *GLS) TrackSingle(id string) (*kardbord.Shipment, error) {
	locale := strings.Split(kardbord.Lang, "_")

	res, err := http.Get(fmt.Sprintf("https://gls-group.eu/app/service/open/rest/%s/%s/rstt002/%s", url.PathEscape(locale[1]), url.PathEscape(locale[0]), url.PathEscape(id)))
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	b, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	j := &response{}
	err = json.Unmarshal(b, j)
	if err != nil {
		return nil, err
	}

	if j.History == nil {
		return nil, nil
	}

	jes := *j.History

	e := make([]kardbord.Event, len(jes))
	for i, je := range jes {
		var status kardbord.Status
		if i == 0 {
			for _, sb := range j.ProgressBar.StatusBar {
				if sb.ImageStatus == "CURRENT" {
					switch sb.Status {
					case "PREADVICE":
						status = kardbord.StatusProcessing
					case "INTRANSIT", "INWAREHOUSE", "INDELIVERY":
						status = kardbord.StatusTransit
					case "DELIVERED":
						status = kardbord.StatusDelivered
					}
					break
				}
			}
		}

		t, err := time.Parse("2006-01-0215:04:05", je.Date+je.Time)
		if err != nil {
			return nil, err
		}

		var location string
		if je.Address.City == "" {
			location = fmt.Sprintf("%s (%s)", je.Address.CountryName, je.Address.CountryCode)
		} else {
			location = fmt.Sprintf("%s, %s (%s)", je.Address.City, je.Address.CountryName, je.Address.CountryCode)
		}

		e[len(jes)-i-1] = kardbord.Event{
			Status:   status,
			Message:  je.Description,
			Time:     t,
			Location: location,
		}
	}

	var weight float64
	for _, info := range j.Infos {
		switch info.Type {
		case "WEIGHT":
			parts := strings.Split(info.Value, " ")
			if parts[1] == "kg" {
				weight, _ = strconv.ParseFloat(parts[0], 64)
			}
		}
	}

	return &kardbord.Shipment{
		Events: e,

		Weight: weight,
	}, nil
}

type response struct {
	History *[]struct {
		Date    string
		Time    string
		Address struct {
			City        string
			CountryName string
			CountryCode string
		}
		Description string `json:"evtDscr"`
	}
	ProgressBar struct {
		StatusBar []struct {
			Status      string
			ImageStatus string
		}
	}
	Infos []struct {
		Type  string
		Value string
	}
}
