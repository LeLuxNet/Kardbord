package all

import (
	"lelux.net/kardbord"
	"lelux.net/kardbord/australiapost"
	"lelux.net/kardbord/cainiao"
	"lelux.net/kardbord/dhl"
	"lelux.net/kardbord/dpd"
	"lelux.net/kardbord/fedex"
	"lelux.net/kardbord/gls"
	"lelux.net/kardbord/hermes"
	"lelux.net/kardbord/laposte"
	"lelux.net/kardbord/oesterreichischepost"
	"lelux.net/kardbord/postnord"
	"lelux.net/kardbord/ups"
	"lelux.net/kardbord/usps"
	"lelux.net/kardbord/yunexpress"
)

var SingleTrackable = map[string]kardbord.SingleTrackable{
	"dhl":                  &dhl.DHL{},
	"dpd":                  &dpd.DPD{},
	"gls":                  &gls.GLS{},
	"hermes":               &hermes.Hermes{},
	"laposte":              &laposte.LaPoste{},
	"oesterreichischepost": &oesterreichischepost.OesterreichischePost{},
	"postnord":             &postnord.PostNord{},
	"ups":                  &ups.UPS{},
}

var MultipleTrackable = map[string]kardbord.MultipleTrackable{
	"australiapost": &australiapost.AustraliaPost{},
	"cainiao":       &cainiao.Cainiao{},
	"fedex":         &fedex.FedEx{},
	"usps":          &usps.USPS{},
	"yunexpress":    &yunexpress.YunExpress{},
}
